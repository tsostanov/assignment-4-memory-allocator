#include <assert.h>
#include <stddef.h>
#include <stdio.h>


#include "mem.h"
#include "mem_internals.h"

#define INIT_SIZE 4096
#define MIN_INIT_SIZE 0
#define REGION_SIZE 4096
#define SMALL_BLOCK_SIZE 1
#define MEDIUM_BLOCK_SIZE sizeof(uint16_t)
#define LARGE_BLOCK_SIZE sizeof(size_t)

void debug(const char *fmt, ...);

void* test_header(int test_number, const char *description, size_t heap_size) {
    debug("Test %d: %s...\n", test_number, description);

    void *heap = heap_init(heap_size);
    assert(heap != NULL);

    return heap;
}

void complete_test(int test_number) {
    heap_term();
    debug("Test %d complete.\n\n", test_number);
}

static struct block_header* get_block_header(void *contents) {
    return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}




static void allocate() {
    void *heap = test_header(1, "Testing memory allocation", INIT_SIZE);

    void *first_block = _malloc(SMALL_BLOCK_SIZE);
    assert(first_block != NULL);

    void *second_block = _malloc(LARGE_BLOCK_SIZE);
    assert(second_block != NULL);

    debug_heap(stderr, heap);

    _free(first_block);
    assert(get_block_header(first_block)->is_free && !get_block_header(second_block)->is_free);
    debug_heap(stderr, heap);
    _free(second_block);
    debug_heap(stderr, heap);
    assert(get_block_header(second_block)->is_free);
    assert(get_block_header(first_block)->next->is_free);

    complete_test(1);
}

static void free_one() {
    void *heap = test_header(2, "Testing freeing one block", INIT_SIZE);


    void *previous_block = _malloc(MEDIUM_BLOCK_SIZE);
    assert(previous_block != NULL);

    void *block = _malloc(MEDIUM_BLOCK_SIZE);
    assert(block != NULL);

    assert(_malloc(MEDIUM_BLOCK_SIZE) != NULL);

    debug_heap(stderr, heap);

    _free(block);
    assert(get_block_header(block)->is_free);
    assert(!get_block_header(previous_block)->is_free && !get_block_header(block)->next->is_free);
    debug_heap(stderr, heap);

    _free(previous_block);
    complete_test(2);
}

static void free_contiguous_blocks() {
    void *heap = test_header(3, "Testing freeing two contiguous blocks", INIT_SIZE);

    assert(_malloc(MEDIUM_BLOCK_SIZE));

    void *first_block = _malloc(MEDIUM_BLOCK_SIZE);
    assert(first_block != NULL);

    void *second_block = _malloc(MEDIUM_BLOCK_SIZE);
    assert(second_block != NULL);

    assert(_malloc(MEDIUM_BLOCK_SIZE));

    assert(_malloc(MEDIUM_BLOCK_SIZE));

    debug_heap(stderr, heap);

    _free(first_block);
    assert(get_block_header(first_block)->is_free && !get_block_header(second_block)->is_free);
    _free(second_block);
    assert(get_block_header(first_block)->is_free && get_block_header(second_block)->is_free);
    debug_heap(stderr,heap);

    complete_test(3);
}
static void free_consecutive_blocks() {
    void *heap = test_header(4, "Testing freeing two consecutive blocks", INIT_SIZE);

    assert(_malloc(MEDIUM_BLOCK_SIZE));

    void *first_block = _malloc(MEDIUM_BLOCK_SIZE);
    assert(first_block != NULL);

    assert(_malloc(MEDIUM_BLOCK_SIZE));

    void *second_block = _malloc(MEDIUM_BLOCK_SIZE);
    assert(second_block != NULL);

    assert(_malloc(MEDIUM_BLOCK_SIZE));

    debug_heap(stderr, heap);

    _free(first_block);
    assert(get_block_header(first_block)->is_free && !get_block_header(second_block)->is_free);
    _free(second_block);
    assert(get_block_header(first_block)->is_free && get_block_header(second_block)->is_free);
    debug_heap(stderr,heap);

    complete_test(4);
}

static void growing_region() {
    void *heap = test_header(5, "Testing growing new region", INIT_SIZE);

    void *first_block = _malloc(REGION_SIZE);
    assert(first_block != NULL);

    debug_heap(stderr, heap);

    void *second_block = _malloc(10 * REGION_SIZE);
    assert(second_block != NULL);

    debug_heap(stderr,heap);

    void *third_block = _malloc(100 * REGION_SIZE);
    assert(third_block != NULL);

    debug_heap(stderr, heap);

    _free(first_block);
    assert(get_block_header(first_block)->is_free && !get_block_header(second_block)->is_free
    && !get_block_header(third_block)->is_free);

    debug_heap(stderr, heap);

    _free(second_block);
    assert(get_block_header(first_block)->is_free && get_block_header(second_block)->is_free
           && !get_block_header(third_block)->is_free);

    debug_heap(stderr, heap);

    _free(third_block);
    assert(get_block_header(first_block)->is_free && get_block_header(second_block)->is_free
           && get_block_header(third_block)->is_free);

    debug_heap(stderr, heap);
    assert((get_block_header(first_block)->next != second_block)
    && (get_block_header(second_block)->next != third_block));

    complete_test(5);
}

static void new_region_in_another_place() {
    void *heap = test_header(6, "Testing new region growing in another place", 5 * INIT_SIZE);

    void *reserved_block_addr = heap + REGION_SIZE;
    void* reserved_block = mmap(reserved_block_addr, REGION_SIZE, PROT_READ | PROT_WRITE,
                                MAP_PRIVATE | 0x20 | MAP_FIXED, -1, 0);
    assert(reserved_block != MAP_FAILED && reserved_block == heap + REGION_SIZE);

    void *first_block = _malloc(REGION_SIZE);
    assert(first_block != NULL);
    assert(first_block != reserved_block);
    debug_heap(stderr, heap);

    void *second_block = _malloc(REGION_SIZE);
    assert(second_block != NULL);
    assert(second_block != reserved_block);
    debug_heap(stderr, heap);

    _free(first_block);
    debug_heap(stderr, heap);

    _free(second_block);
    debug_heap(stderr, heap);


    _free(reserved_block);

    complete_test(6);
}


static void test_min_heap_size(){
    void *min_heap = test_header(7, "Test for minimum heap size", MIN_INIT_SIZE);

    void *block = _malloc(SMALL_BLOCK_SIZE);
    assert(block != NULL);
    debug_heap(stderr, min_heap);

    assert(get_block_header(block)->capacity.bytes == 24);


    _free(block);
    debug_heap(stderr, min_heap);

    complete_test(7);

}


int main() {
    debug("Tests started.\n");
    allocate();
    free_one();
    free_contiguous_blocks();
    free_consecutive_blocks();
    growing_region();
    new_region_in_another_place();
    test_min_heap_size();
    debug("All tests passed.");
    return 0;
}


